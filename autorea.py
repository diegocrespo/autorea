from odf.opendocument import OpenDocumentText
from odf.text import P, H, SoftPageBreak
from odf.style import Style, TextProperties, ParagraphProperties
import json
import datetime
def create_centered_title_style(doc):
    """
    Creates a centered style for the title and adds it to the document.

    Parameters:
    doc (OpenDocumentText): The OpenDocumentText object to which the style will be added.

    Returns:
    Style: The created style object for the title.
    """
    title_style = Style(name="CenteredTitle", family="paragraph")
    title_style.addElement(ParagraphProperties(textalign="center"))
    title_style.addElement(TextProperties(attributes={'fontsize': "24pt", 'fontweight': "bold"}))
    doc.styles.addElement(title_style)
    return title_style


def create_odt_from_json(json_data, filename_prefix='hoja_de_trabajo'):
    """
    Creates an ODT file from the provided JSON data. The ODT file contains a title, a list of problems,
    and an answer key.

    Parameters:
    json_data (str): A JSON formatted string containing the title and examples.

    The structure of json_data should be:
    {
        "titulo": "Title of the worksheet",
        "ejemplos": [
            {"key": "example sentence"},
            ...
        ]
    }
    """
    data = json.loads(json_data)
    doc = OpenDocumentText()

    centered_title_style = create_centered_title_style(doc)
    title = H(outlinelevel=1, stylename=centered_title_style, text=data["titulo"])
    doc.text.addElement(title)

    doc.text.addElement(H(outlinelevel=2, text="Problemas"))
    for index, example in enumerate(data["ejemplos"], start=1):
        for key in example:
            doc.text.addElement(P(text=f"{index}. {example[key]}"))

    doc.text.addElement(SoftPageBreak())

    doc.text.addElement(H(outlinelevel=2, text="Clave De Respuestas"))
    for index, example in enumerate(data["ejemplos"], start=1):
        for key in example:
            doc.text.addElement(P(text=f"{index}. {key}"))

    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime("%m_%d_%Y_%H_%M_%S")

    # Create filename with date and time
    filename = f"{filename_prefix}_{formatted_time}.odt"
    doc.save(filename)

def create_exercise_document(json_data, file_path=None):
    """
    Creates an ODT document from provided JSON data.

    Parameters:
    json_data (str): The JSON string containing the exercises.
    file_path (str, optional): The file path where the ODT file will be saved. Defaults to the current directory.
    """
    # Convert JSON string to dictionary
    exercises = json.loads(json_data)

    # Create a new document
    doc = OpenDocumentText()

    # Create and add centered title style
    title_style = create_centered_title_style(doc)

    # Add main title
    main_title = H(outlinelevel=1, stylename=title_style, text="Ejercicios de Español")
    doc.text.addElement(main_title)

    # Add exercises
    for index, exercise in enumerate(exercises["ejercicios"], start=1):
        # Add the sentence with numbering
        sentence = P(text=f"{index}. {exercise['Oración'].replace('___', '_________')}")
        doc.text.addElement(sentence)

        # Add the options
        for letter, option in zip("abcd", exercise["Opciones"]):
            option_text = P(text=f"{letter}) {option}")
            doc.text.addElement(option_text)

    # Add answer key title
    answer_key_title = H(outlinelevel=2, stylename=title_style, text="Clave de Respuestas")
    doc.text.addElement(answer_key_title)

    # Add answers and explanations
    for index, exercise in enumerate(exercises["ejercicios"], start=1):
        answer = P(text=f"{index}. {exercise['Respuesta Correcta']} - {exercise['Explicación']}")
        doc.text.addElement(answer)

    # Generate file name with date and time
    current_time = datetime.datetime.now().strftime("%m-%d-%Y-%H-%M-%S")
    filename = f"hoja de ejercicios_{current_time}.odt"

    # Save the document
    if file_path:
        full_path = join(file_path, filename)
    else:
        full_path = filename
    doc.save(full_path)
