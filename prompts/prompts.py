def construct_prompt(input_string, keyword_map):
    for placeholder, keyword in keyword_map.items():
        input_string = input_string.replace(placeholder, keyword)
    return input_string
