from openai import OpenAI
from jippity.jippity import *
from autorea import *
import os
# Load the API key and initialize the OpenAI client
api_key = load_api_key(os.path.expanduser('~/auth/api_key.json'))
client = OpenAI(api_key=api_key)

# Read prompts from files
system_prompt = read_file("prompts/system.txt")
assistant_prompt = read_file("prompts/assistant.txt")
worksheet_prompt = read_file("prompts/numbered_worksheets.txt")
worksheet_prompt = read_file("prompts/numbered_worksheets.txt")
choice_prompt = read_file("prompts/multiple_choice.txt")

# Define keyword map and construct the worksheet prompt
keyword_map = {
    "__TIEMPO__": "pretérito",
    "__LINEAS__": "5"
}
choice_map = {
    "__EJEMPLOS__": "5"
}
worksheet_prompt = construct_prompt(worksheet_prompt, keyword_map)

choice_prompt = construct_prompt(choice_prompt, choice_map)
# Initialize history with system and assistant prompts
history = [
    {"role": "system", "content": f'{system_prompt}'}
    ,{"role": "assistant", "content": f'{assistant_prompt}'}
]

# Add user prompt to history
history = create_prompt(history, "user", choice_prompt)

# Create a chat completion
response = create_chat_completion(
    client=client,
    history=history,
    model="gpt-3.5-turbo-1106",
    temperature=0.4,
    response_format={"type": "json_object"}
)

# Print the response
print(response)

# Path where the ODT file will be saved

# Create the document
create_exercise_document(response)
